#include <stdint.h>
struct image create_image(uint32_t width, uint32_t height);
void destroy_image(struct image *image);
uint32_t get_padding_size(uint32_t width);
