#ifndef IMAGE
#define IMAGE
#include <stdint.h>

struct image
{
  uint32_t width, height;
  struct pixel *data;
};
#pragma pack(push, 1)
struct pixel
{
  uint8_t b, g, r;
};
#pragma pack(pop)
#endif
