#include "image.h"
#include <stdio.h>
#include <stdlib.h>


uint32_t get_padding_size(uint32_t width)
{
  return (4 - (width * 3) % 4) % 4;
}
struct image create_image(uint32_t width, uint32_t height)
{
  struct image img = {
      .width = width,
      .height = height,
      .data = malloc(sizeof(struct pixel) * width * height)};

  return img;
}
void destroy_image(struct image *image)
{
  free(image->data);
}
