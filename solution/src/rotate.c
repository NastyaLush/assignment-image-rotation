#include "image.h"
#include "image_work.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

struct image rotate(struct image const source)
{

   struct image image = create_image(source.height, source.width);
   for (size_t i = 0; i < source.height; i++)
   {
      for (size_t j = 0; j < source.width; j++)
      {
         image.data[(source.height - i - 1) + source.height * j] = source.data[i * source.width + j];
      }
   }
   return image;
}
